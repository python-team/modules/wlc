wlc (1.15-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.15

 -- Henrique Pucci <pucci.rique1234@gmail.com>  Sun, 08 Sep 2024 15:06:39 -0300

wlc (1.14-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 1.14.
  * d/watch: Bump version from 3 to 4, no changes needed.
  * d/control:
    - Change build dependencies indentation.
    - Build-Depend on pybuild-plugin-pyproject.
    - Set field Rules-Requires-Root:no.
    - Bump Standards-Version to 4.7.0, no changes needed.
    - Update Description field with latest upstream information.
  * d/wlc.1:
    - Removed font C since it cannot be selected.
    - Remove upstream specific installation step.
    - Fix typos.
    - Added myself in the copyright section.

 -- Sergio de Almeida Cipriano Junior <sergiosacj@riseup.net>  Fri, 10 May 2024 23:29:16 -0300

wlc (1.13-2) unstable; urgency=medium

  * add debian/salsa-ci.yml
  * re-enable tests and add autopkgtest
  * enable upstream's bash-completion

 -- Hans-Christoph Steiner <hans@eds.org>  Mon, 27 Feb 2023 09:42:47 +0100

wlc (1.13-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

  [ Hans-Christoph Steiner ]
  * New upstream version 1.13

 -- Hans-Christoph Steiner <hans@eds.org>  Sat, 04 Feb 2023 20:27:22 +0100

wlc (1.2-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 22:04:18 -0400

wlc (1.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.
  * Bump Standards-Version to 4.4.1.

  [ Michal Čihař ]
  * New upstream release.
  * Bump standards to 4.5.0.

 -- Michal Čihař <nijel@debian.org>  Sat, 22 Feb 2020 13:15:28 +0100

wlc (1.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 4.3.0.

 -- Michal Čihař <nijel@debian.org>  Sun, 17 Feb 2019 09:08:24 +0100

wlc (0.9-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Michal Čihař ]
  * Bump standards to 4.2.1.
  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Thu, 18 Oct 2018 08:59:49 +0200

wlc (0.8-1) unstable; urgency=medium

  * New upstream release.
  * Bump standards to 4.0.0.

 -- Michal Čihař <nijel@debian.org>  Mon, 10 Jul 2017 20:19:17 +0200

wlc (0.7-2) unstable; urgency=medium

  * Remove not needed dependency on six.
  * Add missing python3-pkg-resources dependency.

 -- Michal Čihař <nijel@debian.org>  Fri, 23 Dec 2016 14:05:07 +0100

wlc (0.7-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Fri, 16 Dec 2016 12:57:57 +0100

wlc (0.6-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Tue, 20 Sep 2016 13:45:48 +0200

wlc (0.5-1) unstable; urgency=medium

  * New upstream release.

 -- Michal Čihař <nijel@debian.org>  Mon, 11 Jul 2016 11:10:26 +0200

wlc (0.4-1) unstable; urgency=low

  * Initial packaging, Closes: #824258.

 -- Michal Čihař <nijel@debian.org>  Fri, 08 Jul 2016 11:03:46 +0200
